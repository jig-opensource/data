
# Chrome

[]()  | []()
------|------
__Allgemeine Befehle__ |                       | __Editor Commands__ |
Ctrl Shift u | Unsuspend all Tabs in current window
__Kontextmenü__ |
e | Copy link under cursor



# Sapien PowerShell Studio

[]()  | []() | []() | []()
------|------|------|----- 
__Allgemeine Befehle__ |                       | __Editor Commands__ |
Ctrl Shift s | Save All                        | Ctrl G | Goto Line 
Ctrl Tab | Switch to Next Document Tab         | Ctrl F3 | Find Selection Next
Ctrl Shift Tab | Switch to Prev Document Tab   | Ctrl Shift F3 | Find Selection Previous
Ctrl + F1 | Minimize Ribbon                    | Ctrl Alt F | Find All References
Alt or F12 | Access Ribbon Key Shortcuts       | Ctrl Q | Comment Line
Ctrl + Shift + F | Find in Files               | Ctrl Shift Q | Un-Comment Line 
 |                                             | Ctrl Shift L | Delete Line 
 |                                             | Ctrl Del | Delete To Next Word
__Selection Commands__ |                               | __Moving Commands__ |
Ctrl W | Select Word                                   | Ctrl Page Up | Move To Visible Top
Ctrl Shift ] | Select To Matching Bracket              | Ctrl Page Down | Move To Visible Bottom
Shift Alt Down | Select Block Down                     | Ctrl ] | Move To Matching Bracket
Shift Alt Up | Select Block Up                         | Ctrl Shift Down | Move To Next Modified Line
Shift Alt Left | Select Block Left                     | Ctrl Shift Up | Move To Prev Modified Line
Shift Alt Right | Select Block Right                   | Ctrl Shift Alt Down | Go To Next Occurance
Ctrl Shift Alt Left | Select Block To Previous Word    | Ctrl Shift Alt Up | Go To Pev Occurance
Ctrl Shift Alt Right | Select Block To Next Word       | Ctrl E | Go To Last Edit Position
 |                                                     | Ctrl F12 | Go To Function Declaration
 |                                                     | Shift F12 | Go To Next Function
 |                                                     | Ctrl Shift F12 | Go To Prev Function
__PrimalSense Commands__ |                                   | __Bookmarks__ |
Ctrl Space | PrimalSense Complete Word                       | F2 | Goto Next Bookmark
Ctrl Shift Space | PrimalSense Show Method Parameter Info    | Shift F2 | Goto Previous Bookmark 
Ctrl Alt P | Trigger Custom PrimalSense keyword              | Ctrl F2 | Toggle Bookmark 
 |                                                           | Shift Ctrl F2 | Clear All Bookmarks 
__Go to Pane Commands__ |                | __Collapse__ |
Ctrl Alt P, S | Call Stack               | Ctrl Shift M | Toggle Collapsed Code
Ctrl Alt P, C | Console                  | Ctrl Minus | Collapse All Code Nodes
Ctrl Alt P, D | Debug Output             | Ctrl Plus | Expand All Code Nodes 
Ctrl Alt P, G | Debug Console            | Escape | Collapse Selection
Ctrl Alt P, R | Find Results             | Ctrl R | Create Region 
Ctrl Alt P, F | Function Explorer        | Ctrl Shift 9 | Wrap the selected text in () 
Ctrl Alt P, H | Help                     | Ctrl [ | Wrap the selected text in []
Ctrl Alt P, B | Object Browser           | Ctrl Shift [ | Wrap the selected text in {}
Ctrl Alt P, O | Output                   | Ctrl ' | Wrap the selected text in '' (single quotes)
Ctrl Alt P, M | Performance              | Ctrl Shift ' | Wrap the selected text in "" (double quotes)
Ctrl Alt P, J | Project                  | Ctrl Shift Alt 9 | Wrap the selected text in $() (sub-expression)
Ctrl Alt P, P | Properties               | Ctrl Alt ' | Toggle string quotes
Ctrl Alt P, N | Snippet                  |
Ctrl Alt P, T | Toolbox                  |
Ctrl Alt P, V | Variables                |
Ctrl Alt P, W | Watch                    |
Ctrl Alt P, E | Editor / Document        |
__Other Commands__ |
Ctrl T | Transpose Characters
Ctrl Shift T | Transpose Words
Ctrl Shift Alt T | Transpose Lines
Ctrl B | Toggle Alias
Ctrl K | Insert Snippet
Ctrl Shift K | SurroundsWith Snippet
Ctrl J | Expand Snippet Shortcut
Ctrl Shift E | Insert New Function
Ctrl Shift Alt E | Edit Function
Ctrl Alt J | Rename Object
Ctrl Shift P | Edit Script Parameters
Ctrl Shift H | Qualify cmdlet names
Ctrl Alt H | Unqualify cmdlet names
Ctrl Alt S | Splat Command
__Document Commands__ |
F7 | Build All
Ctrl F7 | Build Package / Build Package and Run
Shift F7 | Build MSI 
Ctrl Shift F7 | Deploy Files
Ctrl Shift F5 | Preview Form 
F5 | Debug 
Ctrl F5 | Run 
Ctrl F8 | Run in Console 
Shift F8 | Run Selection 
F8 | Run Selection in Console 
Ctrl Shift J  | Format Script 
__Navigation Commands__ |
Ctrl Shift - | Navigate Backwards
Ctrl Shift + | Navigate Forward
__Debugging Commands__ |
F10 | Step Over
Shift F11 | Step Out 
Ctrl F10 | Run to Cursor
Ctrl F9 | Toggle Tracepoint
__Designer Commands__ |
Ctrl D | Switch between Design and Editor 
Ctrl Up | Nudge Up
Ctrl Left | Nudge Left
Ctrl Right | Nudge Right 
Ctrl Down | Nudge Down
Shift Up | Height Increase 
Shift Down | Height Decrease
Shift Right | Width Increase
Shift Left | Width Decrease
Ctrl Shift Up | Nudge Height Increase
Ctrl Shift Down | Nudge Height Decrease
Ctrl Shift Right | Nudge Width Increase
Ctrl Shift Left | Nudge Width Decrease
Ctrl B | Bring to Front 
Ctrl Shift B | Send to Back
Enter | Add Default Action Event
Ctrl E | Add Event
Ctrl L | Apply Style
Ctrl Shift L | Create Style
Ctrl T | Create Control Set


