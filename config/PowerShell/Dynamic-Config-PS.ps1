
# Kann so dynamisch geladen werden:
# 'https://gitlab.jig.ch/opensource/data/raw/master/config/PowerShell/Dynamic-Config-PS.ps1' | % { Invoke-WebRequest $_ | select -ExpandProperty Content | Invoke-Expression }

# Hilfe
Function §§Get-Help { 
    Write-Host "Close-Sessions: Alle Sessions schliessen"
}


# Alle Sessions schliessen
Function Close-Sessions { Get-PSSession | Remove-PSSession }

