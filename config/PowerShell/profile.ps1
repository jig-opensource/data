# Installation:
# 
# Default:
# CurrentUserAllHosts
#   # Prüfen
#   notepad $PROFILE.CurrentUserAllHosts
#   # Herunterladen
#   'https://gitlab.jig.ch/opensource/data/raw/master/config/PowerShell/profile.ps1' | % { wget $_ -OutFile $PROFILE.CurrentUserAllHosts }
# 
# Optional:
# CurrentUserCurrentHost
#   # Prüfen
#   notepad $PROFILE.CurrentUserCurrentHost
#   # Herunterladen
#   'https://gitlab.jig.ch/opensource/data/raw/master/config/PowerShell/profile.ps1' | % { wget $_ -OutFile $PROFILE.CurrentUserCurrentHost }


# Hilfe
Function §§Get-Help {   
    Write-Host "Close-Sessions: Alle Sessions schliessen"
}


# Alle Sessions schliessen
Function Close-Sessions { Get-PSSession | Remove-PSSession }
