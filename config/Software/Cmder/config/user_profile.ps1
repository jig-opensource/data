# Use this file to run your own startup commands
# 001
# 	Zeigt die History-Nr. an
# 002
# 	Wenn $SetPromptInfo gesetzt ist, wird dies als Info beim Prompt angezeigt
# 003
# 	Unterscheidet $PSVersionTable.PSVersion.Major
# 004
# 	$SetPromptInfo wird mit '' initialisiert, damit die Shell die Variable kennt und sie einfach genützt werden kann
# 005
# 	Git Repo Status
# 		Alt: "[master ≡ +0 ~1 -6 !]"
# 		Neu: "[master ≡ +0 ~1 -6 !] "
# 
# 	So:
#			$Global:GitPromptSettings.AfterStatus.Text = '] '
# 006
#	Ist neu am Ende:
# 		Import-Module Posh-Git
# 007
# 	Posh-Git müsste später, d.h. hier geladen werden:
# 		c:\Portable\cmder\vendor\profile.ps1
#  Dann müsste ich aber vendor\profile.ps1 bei jedem Update patchen
# 
# !9 	posh-git wird aber automatisch geladen, 
# 		sobald in eine git-Verzeichnis gewechselt wird


## Prompt Customization
<#
.SYNTAX
    <PrePrompt><CMDER DEFAULT>
    λ <PostPrompt> <repl input>
.EXAMPLE
    <PrePrompt>N:\Documents\src\cmder [master]
    λ <PostPrompt> |
#>

[ScriptBlock]$PrePrompt = {
	# 002
	# Wenn $SetPromptInfo gesetzt ist, wird dies als Info beim Prompt angezeigt
	If ([String]::IsNullOrEmpty($SetPromptInfo) -eq $false) {
		# Q https://superuser.com/questions/1259900/how-to-colorize-the-powershell-prompt
		$ESC = [char]27
		# "$ESC[91m$($SetPromptInfo)$ESC[0m • "
		# "$ESC[95m$($SetPromptInfo)$ESC[0m • "
		"$ESC[96m$($SetPromptInfo)$ESC[0m • "
	}
}


# Replace the cmder prompt entirely with this.
# [ScriptBlock]$CmderPrompt = {}

[ScriptBlock]$PostPrompt = {
	# tomtom
	("{0,4} " -f ((Get-History | Measure-Object).Count+1))
}

## <Continue to add your own>

# # Delete default powershell aliases that conflict with bash commands
# if (get-command git) {
#     del -force alias:cat
#     del -force alias:clear
#     del -force alias:cp
#     del -force alias:diff
#     del -force alias:echo
#     del -force alias:kill
#     del -force alias:ls
#     del -force alias:mv
#     del -force alias:ps
#     del -force alias:pwd
#     del -force alias:rm
#     del -force alias:sleep
#     del -force alias:tee
# }


## <Continue to add your own>
# 200221

Microsoft.PowerShell.Utility\Write-Host "Eine Session-Info kann via " -ForegroundColor White -NoNewLine
Microsoft.PowerShell.Utility\Write-Host "`$SetPromptInfo='' " -ForegroundColor Yellow -NoNewLine
Microsoft.PowerShell.Utility\Write-Host "definiert werden" -ForegroundColor White

# Initialisieren, damit die Shell die Variable kennt
$SetPromptInfo = ''

If ($PSVersionTable.PSVersion.Major -lt 5) { Return }


# tomtom
if (Get-Module PSReadline -ErrorAction "SilentlyContinue") {
	Set-PSReadLineOption -HistoryNoDuplicates:$True
	Set-PSReadLineOption -BellStyle None

	# Funktioniert nicht
	# > c:\Scripts\PowerShell\-PSReadLineOption\
	
	# https://stackoverflow.com/a/52818502/4795779
	# $addToHistoryHandler = {
	# 	Param([string]$line)
	# 	Return $False
	# 	$Res = ($line.length -ge 5 -AND $line -notmatch "^get-help|^help|^exit|^dir|^ls|^pwd|^cd ..^\s|^;")
	# 		# Write-Host "addToHistoryHandler: $($Res) [$($Line)]"
	# 	Return $Res
	# }
	# Set-PSReadlineOption -AddToHistoryHandler $addToHistoryHandler	
}

Import-Module jig -DisableNameChecking -ErrorAction SilentlyContinue
Import-Module TomLib -DisableNameChecking -ErrorAction SilentlyContinue


# Eigener Handler, um keine Duplikete in der History zu haben
# 200117 Geht leider nicht
$addToHistoryHandler = {
    Param([string]$Line)

	# Keine Befehle mit 3 oder weniger Zeichen
    If ($Line.Length -le 3) { 
		# Write-Host 'gefunden 1'
		Return [Microsoft.PowerShell.AddToHistoryOption]::SkipAdding 
	} Else {
		$Blacklist = '^\s*(exit|dir|ls|pwd|cd\s+)'
		If ($Line -Match $Blacklist) { 
			# Write-Host 'gefunden 2'
			Return [Microsoft.PowerShell.AddToHistoryOption]::SkipAdding 
		} ElseIf (Get-History | ? CommandLine -eq $Line) {
			# Keine Duplikate
			# Write-Host 'gefunden 3'
			Return [Microsoft.PowerShell.AddToHistoryOption]::SkipAdding
		}
	}
	Return [Microsoft.PowerShell.AddToHistoryOption]::MemoryAndFile
}


# Set-PSReadlineOption -AddToHistoryHandler $AddToHistoryHandler

# Set-PSReadlineOption -AddToHistoryHandler {
	# param($Command)
	# $LastCommand = $(cat $(Get-PSReadlineOption).HistorySavePath -Tail $([regex]::Matches($Command, '\n').Count + 1) -Encoding UTF8) -join ''
	# return $LastCommand -ne ($Command -replace '\n','`')
# }



#		# 006 Posh-Git laden & konfigurieren
#		# Wenn es zu früh geladen wird, fehlt der Prompt
#		# Wenn Posh-Git nicht existiert
#		If (@(Get-Module -Name Posh-Git -ListAvailable).Count -lt 1) {
#			Write-Output 'i Modul Posh-Git könnte noch installiert werden'
#		} else {
#			# Posh-Git ist installiert - allenfalls laden
#			If (@(Get-Module -Name Posh-Git).Count -lt 1) {
#				Microsoft.PowerShell.Utility\Write-Host 'Lade Posh-Git'
#				Import-Module Posh-Git
#				Microsoft.PowerShell.Utility\Write-Host "`r                "
#				$Global:GitPromptSettings.AfterStatus.Text = '] '
#			}
#		}

