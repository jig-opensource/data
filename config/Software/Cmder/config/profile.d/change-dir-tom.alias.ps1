﻿

# TomTom 
# Wechselt das Verzeichnis, wobei auch eine Datei angegeben werden kann
# Mit -Push wird das aktuelle Dir gespeichert
Function Change-Dir() {
	Param(
		[Parameter(Position=0, Mandatory)]
		[String]$TargetDirOrFile,
		[Switch]$Push
	)

	$ThisPath = Get-Location | select -ExpandProperty Path

	If (Test-Path -LiteralPath $TargetDirOrFile -PathType Leaf) {
		$TargetDir = ([IO.Path]::GetDirectoryName($TargetDirOrFile))
	} Else {
		$TargetDir = $TargetDirOrFile
	}
	
	If ($ThisPath -ne $TargetDir) {
		If ($Push) { 
			Push-Location 
			$E = [char]27
			Write-Host 'Current Dir pushed to Stack' -ForegroundColor Magenta -NoNewLine; Start-Sleep -Milliseconds 450; Write-Host "`r$E[2K$E[m"
		}
		Set-Location $TargetDir
	}
}

Function Change-Dir-Push() {
	Param(
		[Parameter(Position=0, Mandatory)]
		[String]$TargetDirOrFile
	)
	Change-Dir $TargetDirOrFile -Push
}

Remove-Item Alias:cd
Set-Alias -Name 'cd' -Value 'Change-Dir'
Set-Alias -Name 'cdp' -Value 'Change-Dir-Push'

