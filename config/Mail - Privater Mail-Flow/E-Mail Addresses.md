# E-Mail Addresses

Default Address for jig.ch
 work@jig.ch

## Forwarders

E-Mail  | Forward to | Sinn
------|------|------
__Akros System-Adressen__ |   | 
akroslog@jig.ch | it@akros.ch   | 
akrostest@jig.ch | thomas.schittli@akros.ch   | 
alarm@akros.jig.ch | _alarmbusu@akros.ch   | 
__work@jig.ch Sammeladresse__ |   | 
work@jig.ch | thomas.schittli@akros.ch   | 
__outlook.com Adressen__ |   | 
t@jig.ch | schittli@outlook.com   | 
tom@jig.ch | schittli@outlook.com   | 
__jig Adressen__ |   | 
nospam@jig.ch | :blackhole:   | Spam-Registrierung
alarmp@jig.ch | work@jig.ch   | Private Alarm-Adresse
antwortfreundesbrief@jig.ch | work@jig.ch   | Antwortadresse Roger
**news-p@jig.ch** |    | News für die persönliche Sichtung
**news-s@jig.ch** |    | News für die Sammlung
dailynews@jig.ch | work@jig.ch   | News: allgemein, täglich
itnews@jig.ch | work@jig.ch   | News: IT
jobnews@jig.ch | work@jig.ch   | News: Jobs
mbanews@jig.ch | work@jig.ch   | News: MBA
news9@jig.ch | work@jig.ch   | News: Prio 9, wichtig
othernews@jig.ch | work@jig.ch   | News: andere news
social-w@jig.ch | work@jig.ch   | 
swenews@jig.ch | work@jig.ch   | 
syno17-ad@jig.ch | work@jig.ch   | 
__Adressen, die gelöscht werden könnten, weil sie nicht automatisch verteilt werden müssen__ |   | 
quora.com@jig.ch | work@jig.ch   | 
zahlungen@jig.ch | work@jig.ch   | 


## E-Mail accounts

E-Mail  | Sinn
------|------
antwortfreundesbrief@jig.ch | 
mailer@jig.ch | 
rogerliebi@jig.ch | 
syno17-ad@jig.ch | 
