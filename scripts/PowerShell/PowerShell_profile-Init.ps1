﻿


## Config
$CfgFile = '.cfg'

Function Get-ScriptFullName {
<#
.SYNOPSIS
	Liefert das Verzeichnis des ablaufenden Scriptes
.EXAMPLE
	$ScriptFullName = Get-ScriptFullName
#>
	$MyInvocation.ScriptName
}


Function Replace-FileExtension($FileName, $NewExt) {
	If (! $NewExt.Contains('.')) { $NewExt = '.' + $NewExt }
	[IO.Path]::Combine([IO.Path]::GetDirectoryName($FileName),
							("{0}{1}" -f [IO.Path]::GetFileNameWithoutExtension($FileName), $NewExt))
}


## Main
$CfgFile = Replace-FileExtension (Get-ScriptFullName) $CfgFile

# Dotsource des Config-Files
Invoke-Expression $([System.IO.File]::ReadAllText("$CfgFile"))

# Alle Funktionen bearbeiten
$Functions | ForEach-Object {
	$_
}

$i=0
