﻿#Requires -Modules JIG

# Ziel: Datei patchen
# Der Code berechnet aus einem CodeBlock einen Regex, um zu erkennen, ob der Block in einer anderen Datei bereits existiert

# >>> ToDo
# Die Optionen des berechneten RegEx in einen PowerShell Regex konvertieren
# Den Regex in einer Testdatei suchen
# Wenn der Text fehlt, ihn der Testdatei zufügen
# Dann ein Init-Script für $PROFILE erstellen
# C:\Users\schittli\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1

# Das Profil Initialisieren
# Funktionen in c:\GitWork\GitLab\OpenSource\daten\scripts\PowerShell\Tom-Aliases.ps1


# Konvertiert einen CodeBlock in einen Regex,
# so dass der CodeBlock einer Datei gefunden wird, auch wenn sich die Leerzeichen unterscheiden
Function Get-Codeblock-Regex($CodeBlock) {
	$Regex = '(?imnx-s:)(?<CodeBlock>'
	
	# Aus dem CodeBlock den Regex berechnen
	$CodeBlock -split '\r\n|\n' | ForEach-Object {
		$Line = [System.Text.RegularExpressions.Regex]::Escape($_)
		
		# Alle Leerzeichen durch \s+ ersetzen
		# $Line = $Line -replace '\s+', '\s+'
		$Line = $Line -replace '\\ |\\t', '\s+'
		
		# Anfangs-Leerzeichen zufügen
		# NewLine ersetzen
		$Line = "\s*$Line\s*(?:\n\r|\n)"
		
		# NewLine ersetzen
		# $Line = "\s+$Line\s+(\n\r|\n)"
		# $Line = $Line -replace '\\n|\\r\\n', '\s*(?:\n\r|\n)'
		
		$Regex += $Line
	}
	$Regex += ')'
	
	# Regex objekt erzeugen
	Get-oRegex $Regex
}

Function File-Has-CodeBlock($FileName, $CodeBlock) {
	$CodeBlockRegex = Get-Codeblock-Regex $CodeBlock
	$FileContent = [IO.File]::ReadAllText($FileName)
	$FileContent -match $CodeBlockRegex
}



Function Get-FileEncoding {
	Param ([string]$FilePath)
	[byte[]]$byte = get-content -Encoding byte -ReadCount 4 -TotalCount 4 -Path $FilePath
	If ($byte[0] -eq 0xef -and $byte[1] -eq 0xbb -and $byte[2] -eq 0xbf) {
		$encoding = 'UTF8'
	} ElseIf ($byte[0] -eq 0xfe -and $byte[1] -eq 0xff) {
		$encoding = 'BigEndianUnicode'
	} ElseIf ($byte[0] -eq 0xff -and $byte[1] -eq 0xfe) {
		$encoding = 'Unicode'
	} ElseIf ($byte[0] -eq 0 -and $byte[1] -eq 0 -and $byte[2] -eq 0xfe -and $byte[3] -eq 0xff) {
		$encoding = 'UTF32'
	} ElseIf ($byte[0] -eq 0x2b -and $byte[1] -eq 0x2f -and $byte[2] -eq 0x76) {
		$encoding = 'UTF7'
	} Else {
		$encoding = 'ASCII'
	}
	Return $encoding
}


Function Append-Text($FileName, $CodeBlock) {
	$Encoding = Get-FileEncoding $FileName
	$Content = Get-Content -Encoding $encoding -LiteralPath $FileName
	$Content += $CodeBlock
	$Content | Set-Content -Path $FileName -Encoding $Encoding
}



$CodeBlock = @'
# Counterpart zu bash which
Function Which($cmd) {
	Get-Command $cmd | Select *
}
'@

$FileName = 'c:\GitWork\GitLab\OpenSource\daten\scripts\PowerShell\test.txt'
$FileName = 'c:\GitWork\GitLab\OpenSource\daten\scripts\PowerShell\test-empty.txt'

If (! (File-Has-CodeBlock $FileName $CodeBlock)) {
	Append-Text $FileName $CodeBlock
}
