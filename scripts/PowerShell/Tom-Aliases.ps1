

# Counterpart zu bash which
Function Which($cmd) {
	Get-Command $cmd | Select *
}


# Alias löschen
Function Remove-Alias($cmd) {
	Remove-Item "Alias:\$cmd"
}



"`nNew-Alias Remove-Alias " | add-content $profile


