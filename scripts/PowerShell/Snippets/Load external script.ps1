# Der Code sucht ein ps1-Script im Verzeichnis des aktuell ausgeführten Scripts


## Config
$EncryptJsonPropertyScript = 'Encrypt-json-Property.ps1'

# Liefert das Verzeichnis des ablaufenden Scriptes
function Get-ScriptDir { "{0}\" -f $($MyInvocation.PSScriptRoot) }

# Holt die erste Datei in $aDir, die den Namen $aScriptName hat
function FindFirstFileName($aDir, $aScriptName) {
	(Get-ChildItem -LiteralPath $aDir -Recurse -Filter $aScriptName | Select-Object -First 1).FullName
}

# Sucht die Datei $aScriptName in $aDir,
# wenn nicht gefunden, wird die datei in den Unterverzeichnissen gesucht
function Get-Script ($aDir, $aScriptName, $ShowError=$true) {
	$ScriptName = [IO.Path]::Combine($aDir, $aScriptName)
	if (Test-Path -Path $ScriptName -PathType Leaf) { return $ScriptName } Else {
		$ScriptName = FindFirstFileName $aDir $aScriptName
		if (Test-Path -Path $ScriptName -PathType Leaf) { return $ScriptName }
	}
	Write-Host "Script fehlt  : $aScriptName"
	Write-Host "In Verzeichnis: $aDir"
	exit
}

### Main
## Config prüfen
$ScriptDir = Get-ScriptDir
$EncryptJsonPropertyScript = Get-Script $ScriptDir $EncryptJsonPropertyScript -ShowError:$false


