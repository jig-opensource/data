﻿#Requires -Version 5

[CmdletBinding()]
param (
	[switch]$ForceInstallModule,
	[switch]$ShowCommands
)


## Config
$Modules = @(
	'FormatPowershellCode'
	'PlatyPS'
	'Pester'
	'PSScriptAnalyzer'
	'Powershell-YAML'
	'PSCodeHealth'
)


Function Show-Pipeline-Progress {
	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
		[PSObject[]]$InputObject,
		[string]$Activity = "Processing items"
	)

	[int]$TotItems = $Input.Count
	[int]$Count = 0
	$Input | ForEach-Object {
		$_
		$Count++
		[int]$percentComplete = ($Count/$TotItems * 100)
		Write-Progress -Activity $Activity -PercentComplete $percentComplete -Status ("Working - " + $percentComplete + "%")
	}
}


Function Get-Module-Version-Online($ModuleName) {
	If ($Module = Find-Module -Name $ModuleName | Sort-Object { [Version]$_.Version } -Descending | Select -First 1) {
		[Version]$Module.Version
	}
}

Function Get-Module-Version-Installed($ModuleName) {
	If ($Module = Get-Module -ListAvailable -Name $ModuleName | Sort-Object { [Version]$_.Version } -Descending | Select-Object -First 1) {
		[Version]$Module.Version
	}
}


# M$ kriegt es nicht hin, mit GitHub kompatibel zu sein,
# also muss Pester parallel installiert werden - mit einer eigenen Verschlüsselung
# https://github.com/pester/Pester/wiki/Installation-and-Update
Function Install-Module-Ignore-CatalogSigned {
	[CmdletBinding()]
	Param(
		[Parameter(ValueFromPipeline)]
		$ModuleName
	)
	process {
		[Version]$VersionLocal = Get-Module-Version-Installed $ModuleName
		[Version]$VersionOnline = Get-Module-Version-Online $ModuleName

		if ($VersionOnline -gt $VersionLocal) {
			try {
				$Splat = @{ Force = ($Script:ForceInstallModule.IsPresent -and $Script:ForceInstallModule)}
				# Versuchen, das Modul zu installieren
				$null = Install-Module -Name $ModuleName -Scope:CurrentUser @Splat -ErrorAction Stop
			}
			catch [System.Exception] {
				# Bei der Exception Catalog Not Signed
				If ($_.FullyQualifiedErrorId -split ',' -contains 'ModuleIsNotCatalogSigned') {
					$GotModuleIsNotCatalogSigned = $true
				}
			}
			If ($GotModuleIsNotCatalogSigned) {
				# Die parallele(!) Installation erzwingen
				$null = Install-Module -Name $ModuleName -Force -SkipPublisherCheck
			}
			
			If ($Script:ShowCommands.IsPresent -and $Script:ShowCommands) {
				Get-Command -Module $ModuleName
			}
		}
	}
}


## Main
$Modules | Show-Pipeline-Progress -Activity 'Installiere Module' | Install-Module-Ignore-CatalogSigned

