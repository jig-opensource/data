﻿
Function Invoke-Executable {
<#
.SYNOPSIS
	Startet eine ausführbare Datei und liefert StdOut, StdErr und den ExitCode
	Returns: custom object
.NOTES   
	004, tom-agplv3@jig.ch, Jehoschua
	https://stackoverflow.com/questions/24370814/how-to-capture-process-output-asynchronously-in-powershell
.EXAMPLE
	$oResult = Invoke-Executable -sExeFile $cmd -cArgs @('8.8.8.8', '-a')
.EXAMPLE
	Invoke-Exe -ExeFile 'cmd' -Arguments @('/c dir')
	Invoke-Executable $exe $MyArgs
.EXAMPLE
	$exe = 'a:\Scripts\PowerShell\-Audio\lib\ffprobe.exe'
	$AudioFileName = 'c:\…\a.mp3'
	$MyArgs = @"
		-show_entries format=duration -v quiet -of csv="p=0" -i "$AudioFileName"
	"@
	Invoke-Executable $exe $MyArgs
.EXAMPLE
	$exe = 'a:\Scripts\PowerShell\-Audio\lib\ffprobe.exe'
	$AudioFileName = 'c:\…\a.mp3'
	$MyArgs=('-show_entries', 'format=duration', '-v quiet', '-of csv="p=0"', "-i `"$AudioFileName`"")
	Invoke-Executable $exe $MyArgs
#>
	[CmdletBinding(SupportsShouldProcess = $true)]
	Param (
		[Parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true, HelpMessage = 'Die ausführbare Datei')]
		[ValidateNotNullOrEmpty()]
		[String]$ExeFile,
		[Parameter(Position = 1, Mandatory = $false, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true, HelpMessage = 'Die Argumente als String Array')]
		[String[]]$Arguments,
		[Parameter(Position = 2, Mandatory = $false, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true, HelpMessage = 'Das auzuführende Verb')]
		[String]$Verb,
		[Parameter(Position = 2, Mandatory = $false, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true, HelpMessage = 'Wenn aktiviert, werden die Argumente nicht auf potentielle Fehler geprüft')]
		[switch]$NoArgsCheck
	)
	begin {
		if ($script:ThisModuleLoaded -eq $true) {
			Get-CallerPreference -Cmdlet $PSCmdlet -SessionState $ExecutionContext.SessionState
		}
		$FunctionName = $MyInvocation.MyCommand.Name
		Write-Verbose "$($FunctionName): Begin."
	
		If (! $NoArgsCheck) {
			$ArgsStr = $Arguments -join ' '
			If ($ArgsStr -match "`'") {
				Write-Warning "Invoke-Executable(): Disable this warning by using -NoArgsCheck: Pfade sollten nicht mit `' sondern mit "" abgegrenzt werden!" -ForegroundColor Red
			}
		}
	}
	process {
		# Setting process invocation parameters.
		$oPsi = New-Object System.Diagnostics.ProcessStartInfo
		Try {
			$oPsi.CreateNoWindow = $true
			$oPsi.UseShellExecute = $false
			$oPsi.RedirectStandardOutput = $true
			$oPsi.RedirectStandardError = $true
			$oPsi.FileName = $ExeFile
			if (! [String]::IsNullOrEmpty($Arguments)) { $oPsi.Arguments = $Arguments }
			if (! [String]::IsNullOrEmpty($Verb)) { $oPsi.Verb = $Verb }
			
			# Creating process object.
			$oProcess = New-Object System.Diagnostics.Process
			$oProcess.StartInfo = $oPsi
			
			# Creating string builders to store stdout and stderr.
			$oStdOutBuilder = New-Object System.Text.StringBuilder
			$oStdErrBuilder = New-Object System.Text.StringBuilder
			
			# Adding event handers for stdout and stderr.
			$sScripBlock = {
				if (! [String]::IsNullOrEmpty($EventArgs.Data)) {
					$Event.MessageData.AppendLine($EventArgs.Data)
				}
			}
			$oStdOutEvent = Register-ObjectEvent -InputObject $oProcess `
															 -Action $sScripBlock -EventName 'OutputDataReceived' `
															 -MessageData $oStdOutBuilder
			$oStdErrEvent = Register-ObjectEvent -InputObject $oProcess `
															 -Action $sScripBlock -EventName 'ErrorDataReceived' `
															 -MessageData $oStdErrBuilder
			
			[Void]$oProcess.Start()
			$oProcess.BeginOutputReadLine()
			$oProcess.BeginErrorReadLine()
			$ExitCode = $oProcess.ExitCode
			[Void]$oProcess.WaitForExit()
		} Finally {
			# Unregistering events to retrieve process output.
			If ($oStdOutEvent) { Unregister-Event -SourceIdentifier $oStdOutEvent.Name; $oStdOutEvent.Dispose() }
			If ($oStdErrEvent) { Unregister-Event -SourceIdentifier $oStdErrEvent.Name; $oStdErrEvent.Dispose() }
			If ($oProcess) { $oProcess.Dispose() }
		}
		
		$oResult = New-Object -TypeName PSObject -Property ([Ordered]@{
				ExeFile	  = $ExeFile;
				Args		  = $Arguments -join " ";
				ExitCode   = $ExitCode # $oProcess.ExitCode;
				StdOut	  = $oStdOutBuilder.ToString().Trim();
				StdErr	  = $oStdErrBuilder.ToString().Trim()
			})
		
		return $oResult
	}
	end {
		Write-Verbose "$($FunctionName): End."
	}
}
