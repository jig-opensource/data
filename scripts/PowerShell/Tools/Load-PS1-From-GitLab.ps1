﻿
Function Load-PS1-From-GitLab {
	<#
	.SYNOPSIS
		Download a Script from jig Public GitHub into a temporary File
	.DESCRIPTION
		An Alias will be created by default. Otherwise, -SetAlias must be set to ''
	.PARAMETER RepoPath
		Optional: Which repository should be used?
		Default: 'opensource/data/raw/master/'
	.PARAMETER ScriptPath
		Which script should be downloaded?
		Example: 'scripts/PowerShell/OS-Win/Invoke-Exe.ps1'
	.PARAMETER SetAlias
		Optional: The Name of the Alias to call the script
		Default: The FileName is used as the Alias name
		'': No Alias will be created
	.EXAMPLE
		Load-PS1-From-GitLab.ps1 'scripts/PowerShell/OS-Win/Invoke-Exe.ps1'
		Invoke-Exe.ps1 will be downloaded
		The Alias Invoke-Exe will be created
	.NOTES
		002, Jehoschua
	#>
	[CmdletBinding(DefaultParameterSetName = 'UseDefaultRepo')]
	Param (
		[parameter(Mandatory = $false, ValueFromPipeline = $true, ParameterSetName = 'UseDefaultRepo')]
		[parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true, ParameterSetName = 'UseUserRepo')]
		[string]$RepoPath = 'opensource/data/raw/master/',
		
		[parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true, ParameterSetName = 'UseDefaultRepo')]
		[parameter(Position = 1, Mandatory = $true, ValueFromPipeline = $true, ParameterSetName = 'UseUserRepo')]
		[string]$ScriptPath,
		
		[parameter(Position = 1, Mandatory = $false, ValueFromPipeline = $true, ParameterSetName = 'UseDefaultRepo')]
		[parameter(Position = 2, Mandatory = $false, ValueFromPipeline = $true, ParameterSetName = 'UseUserRepo')]
		$SetAlias = [IO.path]::GetFileNameWithoutExtension($ScriptPath)
	)
	
	Write-Debug $PsCmdlet.ParameterSetName
	
	
	## Config
	$GitLabRootURI = 'https://gitlab.jig.ch/'
	$GitLabOpensourceDataPath = 'opensource/data/raw/master/'
	
	
	# Liefert true, wenn das Script nur Funktionen beinhaltet
	Function Script-Has-OnlyFunctions($ScriptFile) {
		
		# Liefert alle Root-Elemente
		Function Get-RootNodes($ScriptBlockAst) {
			$predicate = {
				Param ($ast)
				$ast -is [System.Management.Automation.Language.StatementAst] # -and
			}
			# https://docs.microsoft.com/en-us/dotnet/api/system.management.automation.language.ast.findall
			$res = $ScriptBlockAst.FindAll($predicate, $false)
			$res | Select-Object -Property *, @{ Name = 'Type'; Expression = { $_.GetType().Name } }
		}
		
		# Liest $FileName. parst es und gibt das ScriptBlockAst Objekt zurück,
		# also ein AST (Abstract Syntax Tree)
		# https://docs.microsoft.com/en-us/dotnet/api/system.management.automation.language.scriptblockast?view=powershellsdk-1.1.0
		Function Get-ParsedFile($FileName) {
			$Tokens = [System.Management.Automation.Language.Token[]]@()
			$Errors = [System.Management.Automation.Language.ParseError[]]@()
			$ScriptBlockAst = [System.Management.Automation.Language.Parser]::ParseFile($FileName, [ref]$Tokens, [ref]$Errors)
			If ($Errors -and $Errors.Count -gt 0) {
				Write-Error ($Errors -join "`n")
				Exit
			}
			$ScriptBlockAst
		}
		
		$ScriptBlockAst = Get-ParsedFile $FileName
		$ASTRootNodes = Get-RootNodes $ScriptBlockAst
		# Die Funktions-Blöcke entfernen
		[array]$OtherItems = $ASTRootNodes | Where-Object Type -ne 'FunctionDefinitionAst'
		# … und schauen, ob etwas übrig ist
		$OtherItems.count -eq 0
	}
	
	# Liefert alle Funktionsnamen im Root des Scripts
	Function Get-Script-RootFunction-Names($ScriptFile) {
		Function Get-Root-Functions($ScriptBlockAst) {
			$predicate = {
				Param ($ast)
				$ast -is [System.Management.Automation.Language.StatementAst] -and
				$ast.GetType().Name -eq 'FunctionDefinitionAst'
			}
			
			$ScriptBlockAst.FindAll($predicate, $false)
		}
		
		# Liest $FileName. parst es und gibt das ScriptBlockAst Objekt zurück,
		# also ein AST (Abstract Syntax Tree)
		# https://docs.microsoft.com/en-us/dotnet/api/system.management.automation.language.scriptblockast?view=powershellsdk-1.1.0
		Function Get-ParsedFile($FileName) {
			$Tokens = [System.Management.Automation.Language.Token[]]@()
			$Errors = [System.Management.Automation.Language.ParseError[]]@()
			$ScriptBlockAst = [System.Management.Automation.Language.Parser]::ParseFile($FileName, [ref]$Tokens, [ref]$Errors)
			If ($Errors -and $Errors.Count -gt 0) {
				Write-Error ($Errors -join "`n")
				Exit
			}
			$ScriptBlockAst
		}
		
		$ScriptBlockAst = Get-ParsedFile $FileName
		$ASTRootNodes = Get-Root-Functions $ScriptBlockAst
		[array]$ASTRootNodes | Select-Object Name
	}
	
	
	Function New-TempFile($FileNameExt = '.tmp') {
		If (-not $FileNameExt.Contains('.')) { $FileNameExt = '.' + $FileNameExt }
		$FileName = [IO.Path]::Combine([System.IO.Path]::GetTempPath(), ("{0}{1}" -f [string][System.Guid]::NewGuid(), $FileNameExt))
		(New-Item -ItemType File -Path $FileName).FullName
	}
	
	Function Download-File($URI) {
		$WebClient = New-Object System.Net.WebClient
		$file = New-TempFile ([IO.path]::GetExtension($URI))
		$WebClient.DownloadFile($URI, $file)
		$file
	}
	
	
	## Main
	$ScriptPath = "$($GitLabRootURI)$($GitLabOpensourceDataPath)$($ScriptPath)"
	
	If ($File = Download-File $ScriptPath) {
		If (Test-Path -LiteralPath $File) {
			If ([String]::IsNullOrEmpty($SetAlias)) {
				# Call Script
				. $File
			} Else {
				If (Get-Alias | Where-Object Name -EQ "$SetAlias") {
					Throw "Alias existiert bereits: $SetAlias"
				} Else {
					If (Script-Has-OnlyFunctions $File) {
						# Das Script hat keine Main-Funktion, deshalb die Funktion selber zum Alias machen
						[array]$Functions = Get-Script-RootFunction-Names $File
						If ($Functions.count -gt 0) {
							# Das Script und die Fuktionen laden
							. $File
							New-Alias $SetAlias "$($Functions[0])"
						}
					} Else {
						# Das Script hat eine Main-Funktion und ist deshalb selber das Alias
						New-Alias $SetAlias "$File"
						Write-Host "Alias set: $($SetAlias)`Hilfe:"
						Write-Host "Get-Help (Get-Alias $SetAlias | Select-Object -ExpandProperty Definition) -Examples"
					}
				}
			}
		}
	}
}
