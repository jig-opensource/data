#!/bin/bash
# 001, tom-agplv3@jig.ch

# Synology
# Liefert Infos zu den Software-RAID
# inkl. Progress-Informationen beim Rebuild

# erweiterte Infos:
# mdadm --detail /dev/md3
# root@Syno17:/opt/tom# mdadm --detail /dev/md3
# /dev/md3:
#         Version : 1.2
#   Creation Time : Sun Jul 23 22:54:10 2017
#      Raid Level : raid10
#      Array Size : 15618409088 (14894.88 GiB 15993.25 GB)
#   Used Dev Size : 7809204544 (7447.44 GiB 7996.63 GB)
#    Raid Devices : 4
#   Total Devices : 4
#   Persistence : Superblock is persistent
#
#     Update Time : Wed Feb 28 01:02:26 2018
#           State : active, degraded, recovering
#  Active Devices : 2
# Working Devices : 4
#  Failed Devices : 0
#   Spare Devices : 2
#
#          Layout : near=2
#      Chunk Size : 64K
#
#  Rebuild Status : 62% complete
#
#            Name : Syno17:3  (local to host Syno17)
#            UUID : 9286524d:72e2fa07:41bbbde7:9207f48b
#          Events : 478
#
#     Number   Major   Minor   RaidDevice State
#        4       8       35        0      spare rebuilding   /dev/sdc3
#        1       8       51        1      active sync set-B   /dev/sdd3
#        2       8       67        2      active sync set-A   /dev/sde3
#        -       0        0        3      removed
#
#        5       8       83        -      spare   /dev/sdf3


## config
Col1=20

BashColors_Script="bash-colors.sh"
. "${ScriptDir}${BashColors_Script}"

# Sekunden zu h m s
function show_time () {
	num=$1
	min=0
	hour=0
	day=0
	if((num>59));then
		((sec=num%60))
		((num=num/60))
		if((num>59));then
			((min=num%60))
			((num=num/60))
			if((num>23));then
				((hour=num%24))
				((day=num/24))
			else
				((hour=num))
			fi
		else
			((min=num))
		fi
	else
		((sec=num))
	fi

	if [[ $day -eq 0 ]]; then
	        if [[ $sec -eq 0 ]]; then
        	        echo "$hour"h "$min"m
	        else
        	        echo "$hour"h "$min"m "$sec"s
	        fi
	else
	        if [[ $sec -eq 0 ]]; then
        	        echo "$day"d "$hour"h "$min"m
	        else
        	        echo "$day"d "$hour"h "$min"m "$sec"s
	        fi
	fi
}


trim-all() {
	local trimmed="$1"

	# Strip leading spaces.
	while [[ $trimmed == ' '* ]]; do
		trimmed="${trimmed## }"
	done
	# Strip trailing spaces.
	while [[ $trimmed == *' ' ]]; do
		trimmed="${trimmed%% }"
	done

	echo "$trimmed"
}


Get-DiskNo-from-sdx() {
	local sdx="$1"
	letter=${sdx:2:1}
	No=`printf '%d' "'$letter"`
	No="$(($No-96))"
	echo $No
}


# Parameter: mdx oder /dev/mdx
Get-mdadm-detail() {
	local Device="$1"
	# Prefix ergänzen: /dev/
	if [[ ! $Device == /dev/* ]]; then
		Device="/dev/$Device"
	fi
	# echo $Device
	local mdadm=`mdadm --detail $Device`
	# echo $mdadm

	# Array Size : 15618409088 (14894.88 GiB 15993.25 GB)
	local ArraySize=`echo "$mdadm" | gawk 'match($0, /Array Size\s*:.*GiB\s([^\)]+)/, m) {print m[1]}'`
	if [ ! -z "$ArraySize" ]; then
		printf "%*s: %s\n" $Col1 "Array Size" "$ArraySize"
	fi

	# State : clean
        local State=`echo "$mdadm" | gawk 'match($0, /State\s*:\s*([^\n)]+)/, m) {print m[1]}'`
        if [ ! -z "$State" ]; then
		if [[ $State = *"degraded"* ]] || [[ $State = *"resyncing"* ]] || [[ $State = *"recovering"* ]]; then
			printf "${FGRed}%*s: %s${UnsetAll}\n" $Col1 "State" "$State"
		else
			printf "${FGGreen}%*s: %s${UnsetAll}\n" $Col1 "State" "$State"
		fi
        fi

	# Resync Status : 9% complete
	local ResyncState=`echo "$mdadm" | gawk 'match($0, /Resync Status\s*:\s*([^\n)]+)/, m) {print m[1]}'`
	if [ ! -z "$ResyncState" ]; then
		printf "${FGRed}%*s: %s${UnsetAll}\n" $Col1 "Resync Status" "$ResyncState"
	fi

	# exit
}


mdstat-process-line() {
	local line="$1"

	# Nur, wenn line nicht leer ist
	if [[ ! -z "${line// }" ]]; then

		# split string, : and space are delimiters
		IFS=': '
		read -ra items <<<"$line"

		case ${items[0]} in
			Personalities)
				# echo "Überschrift"
			;;

			md*)
				Device=${items[0]}
				echo -e "\nSoftware RAID   : $Device"

				local raidStatus=${items[1]}
				local raidType=${items[2]}
				printf "%*s: %s\n" $Col1 "Status" "$raidStatus"
				printf "%*s: %s\n" $Col1 "Typ" "$raidType"

				# Disk-Nummern bestimmen
				local DiskNumbers=()
				for (( idx=3 ; idx<${#items[@]} ; idx++ )) ; do
					No="$(Get-DiskNo-from-sdx "${items[idx]}")"
					DiskNumbers+=($No)
				done

				# Disk Nummern sortieren
				local Sorted=( $( printf "%s\n" "${DiskNumbers[@]}" | sort -n ) )
				IFS=$'\n'
				local Disks=($Sorted)

				# Disk Nummern ausgeben
				printf "%*s: %s" $Col1 "Disks"
				local init=0
				for disk in ${Disks[*]}; do
					if [ "$init" -eq 0 ]; then
						init=1
						printf "%s" $disk
					else
						printf ", %s" $disk
					fi
				done
				echo -en "\n"

				Get-mdadm-detail "$Device"

			;;

			*)
				recovery=`echo "$line" | gawk 'match($0, /recovery\s*=\s*(\S+)/, a) {print a[1]}'`
				if [ ! -z "$recovery" ]; then
					printf "${FGRed}%*s: %s${UnsetAll}\n" $Col1 "Recovery" "$recovery"

					finish=`echo "$line" | gawk 'match($0, /finish\s*=\s*(\S+)/, a) {print a[1]}'`
					if [ ! -z "$finish" ]; then
						# echo -e "\tFinish  : $finish"
						case $finish in
							*min)
								minuten=${finish%.*}
								sekunden=$(($minuten*60))
								# echo $sekunden
								pendingtime="$(show_time $sekunden)"
								printf "${FGRed}%*s: %s${UnsetAll}\n" $Col1 "Finish in" "$pendingtime"
							;;
							*)
								printf "${FGRed}%*s: %s${UnsetAll}\n" $Col1 "Finish" "$finish"
							;;
						esac
					fi

					speed=`echo "$line" | gawk 'match($0, /speed\s*=\s*(\S+)/, a) {print a[1]}'`
					if [ ! -z "$speed" ]; then
						printf "%*s: %s\n" $Col1 "Speed" "$speed"
					fi
				fi
			;;
		esac
	fi
}




mdstat=`cat /proc/mdstat`

while read -r line; do
	tline="$(trim-all "$line")"
        # Nur, wenn line nicht leer ist
        if [[ ! -z "${line// }" ]]; then
		# echo "Zeile: $tline"
		mdstat-process-line "$tline"
	fi
done <<< "$mdstat"


