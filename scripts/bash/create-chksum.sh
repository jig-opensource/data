#!/bin/bash

# 003

# Erzeugt für jedes Verzeichnis eine Prüfsumme
# 003: Files überspringen

Normal="\e[1;0m"
Red="\e[1;31m"
Green="\e[1;32m"
Yellow="\e[1;33m"
Blue="\e[1;34m"
Magenta="\e[1;35m"
Cyan="\e[1;36m"
White="\e[1;37m"


for d in */
do
	if [ -d "${d}" ] ; then
		# (cd "$d" && $command)
		printf "${Blue}Bearbeite${Normal}: ${d}"
		d=${d::-1}
		
		if [ -f "${d}.sum" ]; then 
			printf "${Green} - Checksumme bereits berechnet${Normal}\n";
		else
			printf "\n";
			# find "${d}" -type f -not -path "*/@eaDir/*" -exec md5sum -b {} \; | sort -k 2 | md5sum > "${d}.sum"
			# Letzten / entfernen
			# d=${d%/}
			# printf "${d}-sum"
			# printf "${d}.sum"
			find "${d}" -type f -not -path "*/@eaDir/*" -exec md5sum -b {} \; | sort -k 2 | md5sum > "${d}.sum"
		fi
	fi
done
