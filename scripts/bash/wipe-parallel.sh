#!/bin/bash
# 001, tom-agplv3@jig.ch

MaxProz=5

for i in $(seq -f "%03g*.*" 0 999)
do
	echo $i
	# find . -type f -name "00*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
	# Dateien mit dem Dateinamenmuster wipen
	find . -type f -name $i -exec shred -n 2 -zu {} 2> /dev/null \; &
	# sleep 1s

	# Sicherstellen, dass wir nicht mehr als MaxProz Prozesse parallel erzeugen
	AnzProz=`ps -ef | grep -c "shred"`
	while [ "$AnzProz" -ge "$MaxProz" ]; do
		sleep 5s
		AnzProz=`ps -ef | grep -c "shred"`
	done
done

# find . -type f -name "00*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "01*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "02*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "03*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "04*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "05*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "06*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "07*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "08*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "09*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "10*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "11*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "12*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "13*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &
# find . -type f -name "14*.*" -exec shred -n 2 -zu {} 2> /dev/null \; &

