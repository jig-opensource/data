#!/bin/bash
# 001, tom-agplv3@jig.ch

# Reset
ResetColors='\033[0m'       # Text Reset

# Formatierungen
SetB='\e[1m'
SetU='\e[4m'
UnsetAll='\e[0m'
UnsetB='\e[21m'
UnsetU='\e[24m'

# Fordergrund
FGDefault='\e[39m'
FGBlack='\e[30m'
FGRed='\e[31m'
FGGreen='\e[32m'
FGYellow='\e[33m'
FGBlue='\e[34m'
FGMagenta='\e[35m'
FGCyan='\e[36m'

FGlGray='\e[37m'
FGdGray='\e[90m'
FGlRed='\e[91m'
FGlGreen='\e[92m'
FGlYellow='\e[93m'
FGlBlue='\e[94m'
FGlMagenta='\e[95m'
FGlCyan='\e[96m'
FGWhite='\e[97m'

# Background
BGDefault='\e[49m'
BGBlack='\e[40m'
BGRed='\e[41m'
BGGreen='\e[42m'
BGYellow='\e[43m'
BGBlue='\e[44m'
BGMagenta='\e[45m'
BGCyan='\e[46m'
BGlGray='\e[47m'
BGdGray='\e[100m'
BGlRed='\e[101m'
BGlGreen='\e[102m'
BGlYellow='\e[103m'
BGlBlue='\e[104m'
BGlMagenta='\e[105m'
BGlCyan='\e[106m'
BGWhyte='\e[107m'

