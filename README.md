# Command line instructions
## Global Setup
```
Git global setup
git config --global user.name "Administrator"
git config --global user.email "admin@example.com"
```

## SSH
### Create a new repository
```
git clone git@gitlab.jig.ch:opensource/scripts.git
cd scripts
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Existing folder
```
cd existing_folder
git init
git remote add origin git@gitlab.jig.ch:opensource/scripts.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.jig.ch:opensource/scripts.git
git push -u origin --all
git push -u origin --tags
```

## HTTP
### Create a new repository
```
git clone http://gitlab.jig.ch/opensource/scripts.git
cd scripts
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Existing folder
```
cd existing_folder
git init
git remote add origin http://gitlab.jig.ch/opensource/scripts.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin http://gitlab.jig.ch/opensource/scripts.git
git push -u origin --all
git push -u origin --tags
```
